package com.ciadnt.estagio2020.project

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import java.lang.reflect.Method

class OperationVolerry {
    private var queue: RequestQueue? = null
    fun setup(context: Context  ) {
        queue = Volley.newRequestQueue(context)
    }
    fun request( url: String, sucessoListener: Response.Listener<JSONObject>,errorListener: Response.ErrorListener,context: Context){
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,sucessoListener,errorListener)
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest)
    }

}