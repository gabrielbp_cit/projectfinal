package com.ciadnt.estagio2020.project

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ciadnt.estagio2020.project.databinding.ListItemCellBinding

class MyAdapter(var onClickPersonagen: OnClickPersonagen, var context: Context): RecyclerView.Adapter<MyViewlHolder>(){

    private var list = ArrayList<Personagem>()

    fun updateList( listLocal : ArrayList<Personagem>){
        list = listLocal
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewlHolder {
        val binding : ListItemCellBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),R.layout.list_item_cell,parent, false)
        return  MyViewlHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewlHolder, position: Int) {
        holder.binding.personagem = list.get(position)
        Glide.with(context).load(list[position].image).into(holder.binding.imagePersonagem)
        holder.binding.onClickListener = onClickPersonagen;
    }

}