package com.ciadnt.estagio2020.project

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.ciadnt.estagio2020.project.databinding.ActivityMainBinding
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private var personagem = ArrayList<Personagem>()
    private var operationVolerry: OperationVolerry = OperationVolerry()
    private lateinit var binding : ActivityMainBinding
    private var sucessolistener: Response.Listener<JSONObject> = Response.Listener<JSONObject>{}
    private var errorListener: Response.ErrorListener = Response.ErrorListener{}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        operationVolerry.setup(this)
        setupList(this)
        updateList(null)
        setupListener()
        operationVolerry.request("https://rickandmortyapi.com/api/character", sucessolistener, errorListener, this)
    }
    private fun setupList(context: Context){
        binding.recyclerListView.adapter = MyAdapter(object : OnClickPersonagen{
            override fun onClick(personagem: Personagem) {
                Toast.makeText(context,"teste " +personagem.name,Toast.LENGTH_SHORT).show()
              Log.d("tag","Teste" +personagem.name)
            }
        }, this)
    }
    fun updateList(v: View?){
        val myListAdapter : MyAdapter = binding.recyclerListView.adapter as MyAdapter
        myListAdapter.updateList(personagem)
    }

    private fun setupListener() {
        sucessolistener = Response.Listener<JSONObject> {response ->
            try{
                var jsonArray: JSONArray = response.getJSONArray("results")
                val myListAdapter : MyAdapter = binding.recyclerListView.adapter as MyAdapter
                for(i in 0..jsonArray.length()) {
                    var info: JSONObject = jsonArray.getJSONObject(i)
                    var name: String = info.getString("name")
                    var status: String = info.getString("status");
                    var gender: String = info.getString("gender")
                    var image: String = info.getString("image")
                    personagem.add(Personagem(name,status,gender,image))
                    myListAdapter.updateList(personagem)
                }
            }catch (e: JSONException){
                e.printStackTrace()
            }
        }
        errorListener = Response.ErrorListener { Log.d("TAG", "onResponse Error") }
    }

    private fun writeResponse(strResponse: JSONObject) {
        runOnUiThread {
            binding.textView.setText(strResponse.getString("name")) }
    }

}
