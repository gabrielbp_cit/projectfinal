package com.ciadnt.estagio2020.project

import androidx.recyclerview.widget.RecyclerView
import com.ciadnt.estagio2020.project.databinding.ListItemCellBinding

class MyViewlHolder(val binding: ListItemCellBinding):  RecyclerView.ViewHolder(binding.root)
